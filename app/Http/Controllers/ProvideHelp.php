<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Package;
use App\Receiver;

class ProvideHelp extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $user;
    protected $package;
    protected $receiver;

    public function __construct(Request $request, Receiver $receiver, User $user, Package $package)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->receiver = $receiver;
        $this->package = $package;
    }


    /**
     * User signs up to a package
     * @params :userId, Package,
     * @return a random user
     */

    public function provideHelp($id, Request $request)
    {
        $package = $request->input('package');
        $package = $this->package->where('amount', $package)->first();
        $user = $this->user->find($id);
        $sponsor = $this->user->where('awaiting_help', true)
            ->where('package_id', 2 )
            ->first();

        $show_sponsor = false;

        if($sponsor == null)
        {

            $sponsor = $this->user->where('is_admin', true)->first();
            $user->package_id = $package->id;
            $user->giving_help = false;
            $user->sponsor = $sponsor;
            $user->save();
            dd($sponsor);

            $show_sponsor = false;
            return redirect()->route('sponsor')
                ->with('sponsors', $sponsor);

        }
//        dd($sponsor);
        $user->sponsor = $sponsor->id;
        $user->package_id = $package->id;
        $user->giving_help = false;
        $user->save();
         return redirect()->route('sponsor')
            ->with(['message', 'Successfully matched to sponsor', 'sponsor', $sponsor]);
    }


    public function getHelp($id, Request $request)
    {
        $user = $this->user->find($id);
        $user->awaiting_help = true;
        flash('You have successfully requested for help, wait to get matched');
        return redirect()->route("dashboard-home")->with('message', 'Success');

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
