<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home()
    {
        return view('dashboard.home');
    }

    public function profile()
    {
        return view('dashboard.profile');

    }

    public function info()
    {
        return view('dashboard.info');

    }

    public function sponsor()
    {
        return view('dashboard.sponsor');

    }

    public function earnings()
    {

    }
}
