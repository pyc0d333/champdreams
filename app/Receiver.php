<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receiver extends Model
{
    protected $fillable = ['user'];

    public function user()
    {
        return $this->hasMany('App\User');
    }
}
