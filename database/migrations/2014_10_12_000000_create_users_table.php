<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('role');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone');
            $table->string('address')->nullable();
            $table->integer('account_number');
            $table->boolean('is_admin')->default(false);
            $table->boolean('is_activated')->default(false);
            $table->boolean('awaiting_help')->default(false);
            $table->boolean('giving_help')->default(false);
            $table->boolean('given_help')->default(false);
            $table->integer('sponsor');
            $table->decimal('total_earnings');
            $table->decimal('total_payments');
            $table->string('referrer_id')->nullable();
            $table->integer('package_id');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
