@extends('layouts.app')
@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Total Investments</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="sm-graph-box">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div id="sparkline_1"></div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="counter-wrap text-right">
                                        <span class="counter-cap"><i class="fa  fa-level-up txt-success"></i></span><span class="counter">23</span><span>%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Total Received</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="sm-graph-box">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div id="sparkline_2"></div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="counter-wrap text-right">
                                        <span class="counter-cap"><i class="fa  fa-level-up txt-success"></i></span><span class="counter">12</span><span>m</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /Row -->

    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Number of Beneficiaries</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="sm-graph-box">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div id="sparkline_6"></div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="counter-wrap text-right">
                                        <span class="counter-cap"><i class="fa  fa-level-down txt-danger"></i></span><span class="counter">1122</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <div class="panel panel-default card-view pa-0">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body pa-0">
                        <div class="sm-data-box bg-red">
                            <div class="row ma-0">
                                <div class="col-xs-5 text-center pa-0 icon-wrap-left">
                                    <i class="icon-diamond txt-light"></i>
                                </div>
                                <div class="col-xs-7 text-center data-wrap-right">
                                    <h6 class="txt-light">Current Package</h6>
                                    <span class="txt-light counter counter-anim">5,000</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()