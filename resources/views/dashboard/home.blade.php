@extends('layouts.app')
@section('content')


    <div class="row">
        {{--@if(Auth::user()->giving_help == false && Auth::user()->is_activated == false )--}}
        @if(Auth::user()->giving_help == false )
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body contact-card">
                        <a href="#">
                            <div class="text-center">
                                <button class="btn  btn-success btn-rounded mt-20 btn-block">PROVIDE HELP</button>
                            </div>
                        </a>

                        <form name="form" role="form" method="post" action="{{url ('provide_help', Auth::user()->id)}}">
                            {{ csrf_field() }}
                            <div class="form-group mt-30 mb-30">
                                <label class="control-label mb-10 text-left">Package</label>
                                <select class="form-control" name="package" required>
                                    <option>------------</option>
                                    <option value="50000">50,000</option>
                                    <option value="100000">100,000</option>
                                    <option value="200000">200,000</option>
                                    <option value="500000">500,000</option>
                                    <option value="1000000">1,000,000</option>
                                </select>
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit To Get Sponsor</span></button>
                            </div>
                        </form>
                        {{Auth::user()->sponsor}}

                    </div>
                </div>
            </div>
        </div>
        @else
            @endif

        @if(Auth::user()->awaiting_help == false)
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div    class="panel-body contact-card">
                        @if(session('message'))
                            <div class="alert-success">{{session('message')}}</div>
                        @endif

                         <form action="{{route('get_help', Auth::user()->id)}}" method="post">
                            {{ csrf_field() }}

                            <a href="#">
                            <div class="text-center">
                                <button type="submit" class="btn btn-danger btn-anim"><i class="icon-rocket"></i><span class="btn-text">GET HELP</span></button>
                             </div>
                        </a>
                        </form>
                    </div>
                </div>

            </div>
                @else
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            @endif
        </div>
        <div class="col-lg-4 col-sm-4 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">What To Do</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="streamline">
                            <div class="sl-item sl-primary">
                                <div class="sl-content">
                                    <p class="txt-dark">Click on  <span class="label label-success">PROVIDE HELP</span> button to get a match</p>
                                </div>
                            </div>

                            <div class="sl-item sl-danger">
                                <div class="sl-content">
                                    <p  class="txt-dark">The information of the person matched will appear</p>
                                </div>
                            </div>

                            <div class="sl-item sl-success">
                                <div class="sl-content">
                                    <p  class="txt-dark">Call the Sponsor to confirm that the information supplied is valid</p>
                                </div>
                            </div>

                            <div class="sl-item sl-warning">
                                <div class="sl-content">
                                    <p  class="txt-dark">Make payment and call sponsor to confirm.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /Row -->
@endsection()