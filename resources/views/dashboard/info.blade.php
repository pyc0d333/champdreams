@extends('layouts.app')
@section('content')


    <div class="row">
        <div class="col-sm-6 col-sm-offset-2">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">YOUR PAYMENT INFORMATION</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <form>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="email_de">Account No.</label>
                                    <input type="text" class="form-control" id="email_de" minlength="10" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="pwd_de">Account  Name</label>
                                    <input type="text" class="form-control" id="pwd_de" required>
                                </div>
                                <div class="form-group mt-30 mb-30">
                                    <label class="control-label mb-10 text-left">Bank Name</label>
                                    <select class="form-control">
                                        <option>------------</option>
                                        <option>Access Bank</option>
                                        <option>FIRST BANK</option>
                                        <option>ECOBANK</option>
                                        <option>GTB</option>
                                        <option>UBA</option>
                                        <option>FIDELITY</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="pwd_de">Phone</label>
                                    <input type="text" class="form-control" id="pwd_de" minlength="11" required>
                                </div>
                                <div class="form-group mb-0">
                                    <button type="button" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">submit</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection()