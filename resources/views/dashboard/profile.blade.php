@extends('layouts.app')
@section('content')


    <div class="row">
        <div class="panel panel-default card-view">
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">warning message</h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <div  class="panel-wrapper collapse in">
                <div  class="panel-body">
                    <img src="dist/img/sweetalert/alert4.png" alt="alert" class="img-responsive model_img" id="sa-warning">
                </div>
            </div>
        </div>
    </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body contact-card">
                        <a href="#">
                            <div class="text-center">
                                <img class="card-user-img" src="dist/img/user.png" alt="user"/>
                                <span class="block card-user-name mt-10">
													Jens Brincker
												</span>
                                <span class="block card-user-email mt-10">
													jens@gmail.com
												</span>
                                <span class="block card-user-phone mt-5 mb-20">
													+123 456 789
												</span>
                                <span class="block label label-danger">Pending</span>

                                <span class="inline-block"> <button class="btn  btn-success btn-rounded mt-20 btn-block">Activate</button>
</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body contact-card">
                        <a href="#">
                            <div class="text-center">
                                <img class="card-user-img" src="dist/img/user.png" alt="user"/>
                                <span class="block card-user-name mt-10">
													Amaka Okey
												</span>
                                <span class="block card-user-email mt-10">
													jens@gmail.com
												</span>
                                <span class="block card-user-phone mt-5 mb-20">
													+123 456 789
												</span>
                                <span class="block label label-success">Activated</span>
                                <span class="inline-block"><button disabled="true" class="btn  btn-success btn-rounded mt-20 btn-block">Activate</button></span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- /Row -->
@endsection()