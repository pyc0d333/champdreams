<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li>
            <a   href="{{url('/home')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-home mr-10"></i>Home<span class="pull-right"></span></a>
        </li>
        <li>
            <a   href="{{url('/matches')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="icon-user mr-10"></i>Matches<span class="pull-right"></span></a>
        </li>
        @if(Auth::user()->giving_help ==  true )
        <li>

            <a   href="{{url('/confirm-payment')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-level-up mr-10"></i>Confirm Payment<span class="pull-right"></span></a>
        </li>
        @else
        @endif
        <li>
            <a   href="{{url('/info')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-credit-card mr-10"></i>Receive Payments<span class="pull-right"></span></a>
        </li>
        <li>
            <a   href="{{url('/earnings')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa  fa-bank mr-10"></i>My Earnings<span class="pull-right"></span></a>
        </li>
        <li>
            <a   href="{{url('/faq')}}" data-toggle="collapse" data-target="#dashboard_dr"><i class="fa fa-info mr-10"></i>Info/FAQ<span class="pull-right"></span></a>
        </li>
        <li>
            <a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>        </li>
    </ul>
</div>