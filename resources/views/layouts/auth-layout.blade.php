<!DOCTYPE html>
<html lang="en">
<head>
   @include('partials.head')
</head>
<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->

<div class="wrapper pa-0 ">

    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0" style="background-color: #081F2C">
        @yield('content')
    </div>

    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->
@include('partials.scripts')
</body>
</html>