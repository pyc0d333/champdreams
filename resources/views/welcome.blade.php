<!DOCTYPE html>
<html>
<head>
    <!-- Site made with Mobirise Website Builder v3.10.1, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v3.10.1, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/logo.png" type="image/x-icon">
    <meta name="description" content="">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&amp;subset=cyrillic,latin,greek,vietnamese">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/mobirise/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/mobirise/css/mbr-additional.css')}}" type="text/css">



</head>
<body>
<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse" id="ext_menu-0">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">

                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="/">CHAMPDREAMS</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger"><span class="mbr-hamburger__line"></span></div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="/">HOME</a></li>
                                <li class="mbr-navbar__item">
                                    {{--<a class="mbr-buttons__link btn text-white" href="#content5-4">ABOUT</a></li>--}}
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="#features1-5">HOW IT WORKS</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="#testimonials1-7">TESTIMONIALS</a></li>
                                {{--<li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="#contacts3-8">CONTACT</a></li>--}}
                                @if (Auth::guest())
                                    <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white"  href="{{ url('/login') }}">LOGIN</a></li>
                                    <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white"  href="{{ url('/register') }}">REGISTER</a></li>

                                 @else
                                    <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white"  href="{{ route('dashboard.home') }}">DASHBOARD</a></li>
                                @endif
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mbr-box mbr-section mbr-section--relative mbr-section--fixed-size mbr-section--full-height mbr-section--bg-adapted mbr-parallax-background" id="header4-1" style="background-image: url(assets/images/bg.jpg);">
    <div class="mbr-box__magnet mbr-box__magnet--sm-padding mbr-box__magnet--center-center mbr-after-navbar">

        <div class="mbr-box__container mbr-section__container container">
            <div class="mbr-box mbr-box--stretched"><div class="mbr-box__magnet mbr-box__magnet--center-center">
                    <div class="row"><div class=" col-sm-8 col-sm-offset-2">
                            <div class="mbr-hero animated fadeInUp">
                                <h1 class="mbr-hero__text">WELCOME TO CHAMPION DREAMS</h1>
                                <p class="mbr-hero__subtext">Making your dreams of a financially independent life come true<br><br></p>
                            </div>
                            <div class="mbr-buttons btn-inverse mbr-buttons--center"><a class="mbr-buttons__btn btn btn-lg btn-danger animated fadeInUp delay" href="{{route ('register')}}">Join Us</a> </div>
                        </div></div>
                </div></div>
        </div>

    </div>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size" id="content4-2" style="background-color: rgb(255, 255, 255);">

    <div class="mbr-section__container mbr-section__container--std-top-padding mbr-section__container--sm-bot-padding mbr-section-title container" style="padding-top: 31px;">
        <div class="mbr-header mbr-header--center mbr-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbr-header__text">HOW IT WORKS</h3>

            </div>
        </div>
    </div>
    <div class="mbr-section__container container">
        <div class="mbr-section__row row">
            <div class="mbr-section__col col-xs-12 col-lg-2-4 col-md-4 col-sm-6">

                <div class="mbr-section__container mbr-section__container--middle">
                    <div class="mbr-header mbr-header--reduce mbr-header--center mbr-header--wysiwyg">
                        <h3 class="mbr-header__text"><em>&nbsp;Provide Help</em></h3>
                    </div>
                </div>
                <div class="mbr-section__container mbr-section__container--last" style="padding-bottom: 62px;">
                    <div class="mbr-article mbr-article--wysiwyg">
                        <p>Our system matches you with another person whom you're to pay to.&nbsp;<br>Contact the person to verify Identity before making payment</p>
                    </div>
                </div>

            </div>
            <div class="mbr-section__col col-xs-12 col-lg-2-4 col-md-4 col-sm-6">

                <div class="mbr-section__container mbr-section__container--middle">
                    <div class="mbr-header mbr-header--reduce mbr-header--center mbr-header--wysiwyg">
                        <h3 class="mbr-header__text">Confirm Payment<br><br></h3>
                    </div>
                </div>
                <div class="mbr-section__container mbr-section__container--last" style="padding-bottom: 62px;">
                    <div class="mbr-article mbr-article--wysiwyg">
                        <p>Confirm your payment from your dashboard and also call your sponsor to confirm your payment</p>
                    </div>
                </div>

            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="mbr-section__col col-xs-12 col-lg-2-4 col-md-4 col-sm-6">

                <div class="mbr-section__container mbr-section__container--middle">
                    <div class="mbr-header mbr-header--reduce mbr-header--center mbr-header--wysiwyg">
                        <h3 class="mbr-header__text">Get paid</h3>
                    </div>
                </div>
                <div class="mbr-section__container mbr-section__container--last" style="padding-bottom: 62px;">
                    <div class="mbr-article mbr-article--wysiwyg">
                        <p>Click on the get help button to be paired with 2 other people who will each pay the amount of the package you chose</p>
                    </div>
                </div>

            </div>
            <div class="clearfix visible-md-block"></div>
            <div class="mbr-section__col col-xs-12 col-lg-2-4 col-md-4 col-sm-6 col-lg-offset-0 col-md-offset-2">

                <div class="mbr-section__container mbr-section__container--middle">
                    <div class="mbr-header mbr-header--reduce mbr-header--center mbr-header--wysiwyg">
                        <h3 class="mbr-header__text">Receive Payments</h3>
                    </div>
                </div>
                <div class="mbr-section__container mbr-section__container--last" style="padding-bottom: 62px;">
                    <div class="mbr-article mbr-article--wysiwyg">
                        <p>Now you can get paid 200% of whatever package you paid for.<br><br>Confirm receipt of payments from your dashboard.</p>
                    </div>
                </div>

            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="mbr-section__col col-xs-12 col-lg-2-4 col-md-4 col-sm-6 col-md-offset-0 col-sm-offset-3">

                <div class="mbr-section__container mbr-section__container--middle">
                    <div class="mbr-header mbr-header--reduce mbr-header--center mbr-header--wysiwyg">
                        <h3 class="mbr-header__text">Provide Help Again</h3>
                    </div>
                </div>
                <div class="mbr-section__container mbr-section__container--last" style="padding-bottom: 62px;">
                    <div class="mbr-article mbr-article--wysiwyg">
                        <p>Choose a new package, provide help and keep earning more and more!<br>This is just a repeat of the same process.</p>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size mbr-parallax-background" id="testimonials1-4" style="background-image: url(assets/images/bg3.jpg);">
    <div>
        <div class="mbr-overlay" style="opacity: 0.1; background-color: rgb(34, 34, 34);"></div>
        <div class="mbr-section__container mbr-section__container--std-padding container" style="padding-top: 62px; padding-bottom: 62px;">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="mbr-section__header">WHAT OUR FANTASTIC USERS SAY</h2>
                    <ul class="mbr-reviews mbr-reviews--wysiwyg row">
                        <li class="mbr-reviews__item col-xs-12 col-sm-6 col-md-4">
                            <div class="mbr-reviews__text"><p class="mbr-reviews__p">“Click on the get help button to be paired with 2 other people who will each pay the amount of the package you chose"</p></div>
                            <div class="mbr-reviews__author mbr-reviews__author--short">
                                <div class="mbr-reviews__author-name">ABA. RA</div>

                            </div>
                        </li><li class="mbr-reviews__item col-xs-12 col-sm-6 col-md-4">
                            <div class="mbr-reviews__text"><p class="mbr-reviews__p">“Click on the get help button to be paired with 2 other people who will each pay the amount of the package you chose”</p></div>
                            <div class="mbr-reviews__author mbr-reviews__author--short">
                                <div class="mbr-reviews__author-name">DR. UCHE</div>

                            </div>
                        </li><li class="mbr-reviews__item col-xs-12 col-sm-6 col-md-4">
                            <div class="mbr-reviews__text"><p class="mbr-reviews__p">“Click on the get help button to be paired with 2 other people who will each pay the amount of the package you chose”</p></div>
                            <div class="mbr-reviews__author mbr-reviews__author--short">
                                <div class="mbr-reviews__author-name">USMAN MOHAMMED</div>

                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size mbr-parallax-background" id="pricing-table1-5" style="background-image: url(assets/images/landscape-2000x1501-76.jpg);">

    <div class="mbr-section__container mbr-section__container--std-top-padding container" style="padding-top: 93px; margin-bottom: 0px;">
        <div class="row">
            <div class="mbr-plan col-xs-12 mbr-plan--danger mbr-plan--favorite mbr-plan--first col-lg-2-4 col-md-4 col-sm-6">
                <div class="mbr-plan__box">
                    <div class="mbr-plan__header">
                        <div class="mbr-header mbr-header--reduce mbr-header--center mbr-header--wysiwyg">
                            <h3 class="mbr-header__text">STANDARD</h3>
                        </div>
                    </div>
                    <div class="mbr-plan__number">
                        <div class="mbr-number mbr-number--price">
                            <div class="mbr-number__num">
                                <div class="mbr-number__group">
                                    <sup class="mbr-number__left">50,000</sup><span class="mbr-number__value"></span>
                                </div>
                            </div>
                            <div class="mbr-number__caption"></div>
                        </div>
                    </div>
                    <div class="mbr-plan__details"></div>

                </div>
            </div>
            <div class="mbr-plan col-xs-12 mbr-plan--success col-lg-2-4 col-md-4 col-sm-6">
                <div class="mbr-plan__box">
                    <div class="mbr-plan__header">
                        <div class="mbr-header mbr-header--reduce mbr-header--center mbr-header--wysiwyg">
                            <h3 class="mbr-header__text">BUSINESS</h3>
                        </div>
                    </div>
                    <div class="mbr-plan__number">
                        <div class="mbr-number mbr-number--price">
                            <div class="mbr-number__num">
                                <div class="mbr-number__group">
                                    <sup class="mbr-number__left">100,000</sup><span class="mbr-number__value"></span>
                                </div>
                            </div>
                            <div class="mbr-number__caption"></div>
                        </div>
                    </div>
                    <div class="mbr-plan__details"></div>

                </div>
            </div>
            <div class="mbr-plan col-xs-12 mbr-plan--danger mbr-plan--favorite col-lg-2-4 col-md-4 col-sm-6">
                <div class="mbr-plan__box">
                    <div class="mbr-plan__header">
                        <div class="mbr-header mbr-header--reduce mbr-header--center mbr-header--wysiwyg">
                            <h3 class="mbr-header__text">PREMIUM</h3>
                        </div>
                    </div>
                    <div class="mbr-plan__number">
                        <div class="mbr-number mbr-number--price">
                            <div class="mbr-number__num">
                                <div class="mbr-number__group">
                                    <sup class="mbr-number__left">250,000</sup><span class="mbr-number__value"></span>
                                </div>
                            </div>
                            <div class="mbr-number__caption"></div>
                        </div>
                    </div>
                    <div class="mbr-plan__details"><ul><li></li></ul></div>

                </div>
            </div>
            <div class="mbr-plan col-xs-12 mbr-plan--warning col-lg-2-4 col-md-4 col-sm-6 col-lg-offset-0 col-md-offset-2">
                <div class="mbr-plan__box">
                    <div class="mbr-plan__header">
                        <div class="mbr-header mbr-header--reduce mbr-header--center mbr-header--wysiwyg">
                            <h3 class="mbr-header__text">ULTIMATE</h3>
                        </div>
                    </div>
                    <div class="mbr-plan__number">
                        <div class="mbr-number mbr-number--price">
                            <div class="mbr-number__num">
                                <div class="mbr-number__group">
                                    <sup class="mbr-number__left">300,000</sup><span class="mbr-number__value"></span>
                                </div>
                            </div>
                            <div class="mbr-number__caption"></div>
                        </div>
                    </div>
                    <div class="mbr-plan__details"><ul><li></li></ul></div>

                </div>
            </div>
            <div class="mbr-plan col-xs-12 mbr-plan--success mbr-plan--last col-lg-2-4 col-md-4 col-sm-6 col-md-offset-0 col-sm-offset-3">
                <div class="mbr-plan__box">
                    <div class="mbr-plan__header">
                        <div class="mbr-header mbr-header--reduce mbr-header--center mbr-header--wysiwyg">
                            <h3 class="mbr-header__text">EXTENDED</h3>
                        </div>
                    </div>
                    <div class="mbr-plan__number">
                        <div class="mbr-number mbr-number--price">
                            <div class="mbr-number__num">
                                <div class="mbr-number__group">
                                    <sup class="mbr-number__left">1,000,000</sup><span class="mbr-number__value"></span>
                                </div>
                            </div>
                            <div class="mbr-number__caption"></div>
                        </div>
                    </div>
                    <div class="mbr-plan__details"></div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size" id="contacts3-6" style="background-color: rgb(60, 60, 60);">

    <div class="mbr-section__container container">
        <div class="mbr-contacts mbr-contacts--wysiwyg row" style="padding-top: 45px; padding-bottom: 45px;">
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <p class="mbr-contacts__text"><strong>ADDRESS</strong><br>
                            1234 Street Name<br>
                            City, AA 99999<br><br>
                            <strong>CONTACTS</strong><br>
                            Email: support@champdreams.com<br>
                            Phone: +1 (0) 000 0000 001<br>
                            Fax: +1 (0) 000 0000 002</p>
                    </div>
                    <div class="col-sm-6"><ul class="mbr-contacts__list"></ul></div>
                </div>
            </div>
            <div class="mbr-contacts__column col-sm-4" data-form-type="formoid">
                <div data-form-alert="true">
                    <div class="hide" data-form-alert-success="true">Thanks for filling out form!</div>
                </div>
                <form action="https://mobirise.com/" method="post" data-form-title="MESSAGE">
                    <input type="hidden" value="bttdK3hQB94p3zYwh/uXnuat1B/O34mQyXgv2encsP5BkdCyZ6AN3s5KqcOT7ubQcmTQICM+7iXx918DoxlMv8ylA4Y5wwniFvdIGXOQJX3JGtaXInkNsvmbq/FVwZ6e" data-form-email="true">

                    <div class="form-group">
                        <input type="email" class="form-control input-sm input-inverse" name="email" required="" placeholder="Email*" data-form-field="Email">
                    </div>

                    <div class="form-group">
                        <textarea class="form-control input-sm input-inverse" name="message" rows="4" placeholder="Message" data-form-field="Message"></textarea>
                    </div>
                    <div class="mbr-buttons mbr-buttons--right btn-inverse"><button type="submit" class="mbr-buttons__btn btn btn-danger">CONTACT US</button></div>
                </form>
            </div>
        </div>
    </div>
</section>

<footer class="mbr-section mbr-section--relative mbr-section--fixed-size" id="footer1-7" style="background-color: rgb(68, 68, 68);">

    <div class="mbr-section__container container">
        <div class="mbr-footer mbr-footer--wysiwyg row" style="padding-top: 36.900000000000006px; padding-bottom: 36.900000000000006px;">
            <div class="col-sm-12">
                <p class="mbr-footer__copyright">Copyright (c) 2017 Champion Dreams. <a class="mbr-footer__link text-gray" href="https://mobirise.com/">Terms of Use</a>  | <a class="mbr-footer__link text-gray" href="https://mobirise.com/">Privacy Policy</a></p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="{{ asset ('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset ('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{ asset ('assets/smooth-scroll/SmoothScroll.js')}}"></script>
<script src="{{ asset ('assets/jarallax/jarallax.js')}}"></script>
<!--[if lte IE 9]>
<script src="{{ asset ('assets/jquery-placeholder/jquery.placeholder.min.js')}}"></script>
<![endif]-->
<script src="{{ asset ('assets/mobirise/js/script.js')}}"></script>
<script src="{{ asset ('assets/formoid/formoid.min.js')}}"></script>


</body>
</html>