<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'SiteController@home')->name('dashboard-home');

Route::get('', function () {
    return view('welcome');
});


Route::get('/profile', 'SiteController@profile');

Route::get('/info', 'SiteController@info');

Route::get('/sponsor', 'SiteController@sponsor')->name('sponsor');

Route::get('/confirm-payment', function (){
    return view('dashboard.confirm-payment');
});


Route::get('/earnings', function (){
    return view('dashboard.earnings');
});

Route::get('/matches', function (){
    return view('dashboard.matches');
});

Auth::routes();



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::post('/provide_help/{id}', 'ProvideHelp@provideHelp')->name('provide_help');
Route::post('/get_help/{id}', 'ProvideHelp@getHelp')->name('get_help');
Route::post('/payment_info/{id}', 'UserController@payment_info')->name('add_payment_info');